import { Component, OnInit } from '@angular/core';
import { BikesService } from '../bikes.service';
import { Bike } from '../Interface/bike';

@Component({
  selector: 'app-bikes-list',
  templateUrl: './bikes-list.component.html',
  styleUrls: ['./bikes-list.component.styl']
})

export class BikesListComponent implements OnInit {

  public bikesList: Bike[];

size = 5;
page= 0;

listPages = [] //Lista de paginas
  constructor(private bikesService: BikesService){} //bikesService ---> el nombre que quieres darle
    
 
  ngOnInit() {  //Instrucciones que quieres que se carguen automaticamente cuando el usuario ingrese
   this.initPagination(this.size);
  }

  initPagination(page: number): void {
    this.bikesService.queryBikes({
      'size': this.size,
      'page': page
    })
   .subscribe((res: any) => {
     this.bikesList = res; //res.content
     this.formatPages(res.totalPages);
     console.log('Response data ', res); //res.content
     this.formatPages(res.totalPages);
   },
   error => console.error('Error ', error))
  }



  deleteItem(id: String){
    console.warn('ID', id);
    this.bikesService.deleteItem(id)
      .subscribe(res => {
        console.warn('Item deleted ok---');
        this.ngOnInit
      })
  }

private formatPages(countPages: number): void {
  this.listPages = [];
  for(let i=0; i < countPages; i++){
    this.listPages.push(i);
  }
}

}







