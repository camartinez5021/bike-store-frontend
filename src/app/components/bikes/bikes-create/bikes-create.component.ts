import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Bike } from '../Interface/bike';
import { BikesService } from '../bikes.service';

@Component({
  selector: 'app-bikes-create',
  templateUrl: './bikes-create.component.html',
  styleUrls: ['./bikes-create.component.styl']
})
export class BikesCreateComponent implements OnInit {
  BikeFormGroup: FormGroup;
  bike: Bike[];

  statusSearchSerial: boolean = false;

  formSearchBike = this.formBuilder.group({
      serial: ['']
  });


  constructor(private formBuilder: FormBuilder, private bikesService: BikesService) {
    this.BikeFormGroup = this.formBuilder.group({
      model: ['', Validators.compose([
        Validators.required,
        Validators.maxLength(4)
      ])],
      price: [''],
      serial: ['']
    });
   }

  ngOnInit() {
  }

  saveBike() {
    console.log('DATOS', this.BikeFormGroup.value);
    this.bikesService.saveBike(this.BikeFormGroup.value)
    .subscribe(res => {
        console.log('SAVE OK ', res);
        
    }, error => {
     
      console.error('Error', error);
    });
  }
/*
  searchBike() {
    console.warn('Data', this.formSearchBike.value);
    this.bikesService.getBikeBySerial(this.formSearchBike.value.serial)
    .subscribe(res => {
      console.warn('response bike by Serial ',res);
      this.bike = res;
      if(res.length > 0){
        this.statusSearchSerial = false;
      }else {
        this.statusSearchSerial = true;
      }
    });
  }
  */

}
