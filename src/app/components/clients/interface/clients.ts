export interface IClients {
    id?: number,
    name: string,
    document: string,
    email: string,
    phoneNumber: string,
    documentType: string,
    image?: any,
    imageContentType?: any
}