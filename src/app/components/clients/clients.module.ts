import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ClientsListComponent } from './clients-list/clients-list.component';
import { ClientsCreateComponent } from './clients-create/clients-create.component';
import { ClientsUpdateComponent } from './clients-update/clients-update.component';
import { ClientsRoutingModule } from './clients-routing.module';
import { ReactiveFormsModule } from '@angular/forms';



@NgModule({
  declarations: [ClientsListComponent, ClientsCreateComponent, ClientsUpdateComponent],
  imports: [
    CommonModule, ClientsRoutingModule, ReactiveFormsModule
  ]
})
export class ClientsModule { }
