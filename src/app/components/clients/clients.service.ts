import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { IClients } from './interface/clients';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { map } from 'rxjs/operators';
import { createRequestParams } from 'src/app/utils/request.utils';

@Injectable({
  providedIn: 'root'
})
export class ClientsService {

  constructor(private http: HttpClient) { }

  /**
   * Llamar datos del backend
   * @param IClients
   */
  public queryClients(req?: any): Observable<IClients[]>{
    let params = createRequestParams(req);
    return this.http.get<IClients[]>(`${environment.END_POINT}/api/clients`, {params: params})
    .pipe(map(res => {
      return res;
    }));
  }

  /**
   * Creacion del metodo guardar para create
   * @param IClients
   */
  public saveClients(IClients: IClients): Observable<IClients>{
    return this.http.post<IClients>(`${environment.END_POINT}/api/clients`, IClients)
    .pipe(map(res => {
      return res;
    }));
  }

  public findClientByDocumentNumber(req?: any): Observable<IClients[]>{
    let params = createRequestParams(req);
    return this.http.get<IClients[]>(`${environment.END_POINT}/api/clients/find-by-document`,{params: params})
    .pipe(map(res => {
      return res;
    }));
  }
}
