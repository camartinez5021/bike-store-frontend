import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder } from '@angular/forms';
import { IClients } from '../interface/clients';
import { ClientsService } from '../clients.service';

@Component({
  selector: 'app-clients-create',
  templateUrl: './clients-create.component.html',
  styleUrls: ['./clients-create.component.styl']
})
export class ClientsCreateComponent implements OnInit {
  ClientsFormGroup: FormGroup;
  clients: IClients[];

  constructor(private formBuilder: FormBuilder, private clientsService: ClientsService) {
    this.ClientsFormGroup=this.formBuilder.group({
      name:[''],
      document:[''],
      email:[''],
      phoneNumber:[''],
      documentType:['']
    });
   }

  ngOnInit() {
  }

  saveClients(){
    console.warn('DATOS', this.ClientsFormGroup.value);
    this.clientsService.saveClients(this.ClientsFormGroup.value)
    .subscribe(res => {
      console.warn('SAVE OK ', res);
    }, error => {
      console.error('Error ', error)
    });
  }

}
