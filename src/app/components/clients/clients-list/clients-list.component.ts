import { Component, OnInit } from '@angular/core';
import { IClients } from '../interface/clients';
import { ClientsService } from '../clients.service';

@Component({
  selector: 'app-clients-list',
  templateUrl: './clients-list.component.html',
  styleUrls: ['./clients-list.component.styl']
})
export class ClientsListComponent implements OnInit {

  public clientsList: IClients[];

  constructor(private clientsService: ClientsService) { }

  ngOnInit() {
  this.clientsService.queryClients() 
  .subscribe(res => {
    this.clientsList = res;
    console.warn('Response data ', res);
  },
  error => console.error('Error ', error)) 
  };

}
