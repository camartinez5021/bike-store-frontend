import { Component, OnInit } from '@angular/core';
import { AuthService } from 'src/app/auth/auth.service';
import { AccountService } from 'src/app/auth/account.service';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.styl']
})
export class ProfileComponent implements OnInit {
  userName: String;
  userActivated: boolean;
  userEmail: String;
  userLogin: String
  userAuthorities: String[]

  constructor(private accountService: AccountService) { }

  ngOnInit(): void {
    this.userName = this.accountService.getUserName();
    this.userActivated = this.accountService.getUseractivated();
    this.userEmail = this.accountService.getUserEmail();
    this.userLogin = this.accountService.getUserLogin();
    this.userAuthorities = this.accountService.getUserAuthorities();
  }

}
