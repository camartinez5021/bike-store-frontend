import { Component, OnInit } from '@angular/core';
import { AuthService } from 'src/app/auth/auth.service';
import { AccountService } from 'src/app/auth/account.service';
import { Account } from 'src/app/auth/auth-shared/models/account.model';
import { LoginService } from 'src/app/auth/login/login.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.styl']
})
export class HeaderComponent implements OnInit {
  account: String;

  constructor(private loginService: LoginService,
    private accountService: AccountService,
    private router: Router) { }

  ngOnInit(){
    this.account = this.accountService.getUserName();
  }

  Logout(): void {
    this.loginService.logout()
    this.router.navigate(['/login'])
  }

  

  
}
