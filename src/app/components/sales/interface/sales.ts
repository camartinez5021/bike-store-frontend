import { Bike } from '../../bikes/Interface/bike';
import { IClients } from '../../clients/interface/clients';

export interface ISales{
date?: Date,
clientId?: Number,
clientName?: String,
bikeId?: Number,
bikeSerial?: String,
bike?: Bike,
client?: IClients

}