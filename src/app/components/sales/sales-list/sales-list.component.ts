import { Component, OnInit } from '@angular/core';
import { SalesService } from '../sales.service';
import { BikesService } from '../../bikes/bikes.service';
import { Bike } from '../../bikes/Interface/bike';
import { ISales } from '../interface/sales';
import { IClients } from '../../clients/interface/clients';
import { ClientsService } from '../../clients/clients.service';
import { FormBuilder } from '@angular/forms';

@Component({
  selector: 'app-sales-list',
  templateUrl: './sales-list.component.html',
  styleUrls: ['./sales-list.component.styl']
})

export class SalesListComponent implements OnInit {
  total: number = 0;

  clientsList: IClients[] = [];
  bikesList: Bike[];
  clientSelectedTemp: IClients;
  salesListCarsTwo: ISales[] = [];
  salesH: ISales[];

  listPages=[]; //Lista de paginas para botones

  statusSearchDocument: boolean = false;

  formSearchClients = this.formBuilder.group({
    document: ['']
  });

  constructor(
    private formBuilder: FormBuilder,
    private salesService: SalesService,
    private bikesService: BikesService,
    private clientsService: ClientsService,
  ) { }

  ngOnInit() {
    this.bikesService.queryBikes()
    .subscribe(res => {
      this.bikesList = res;
    });
  }

  
  selectClient(client: any): void{
    console.warn('Selected Client ', client)
    this.clientSelectedTemp = client;
  }

  addToCar(bike: Bike){
    this.total = this.total + bike.price;
    console.warn('Selected buy ', bike);
    this.salesListCarsTwo.push({
      clientId: this.clientSelectedTemp.id,
      clientName: this.clientSelectedTemp.name,
      bikeId: bike.id,
      bikeSerial: bike.serial,
      bike: bike
    });
  }

  searchClient(event: any): void {
    console.warn('Event ', event)
    console.warn('Key up');
    //this.clientsService.queryClients({
      this.clientsService.findClientByDocumentNumber({
      'document': event.query
    })
    .subscribe(res => {
      this.clientsList = res;
      console.warn('Client view ', this.clientsList)
    })
}

/*
confirmSale(sales: ISales){
  console.warn('Enviar datos al backend', this.salesH);
  this.salesH.push(sales);
}
*/

}
