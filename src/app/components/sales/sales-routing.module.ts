import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { SalesListComponent } from './sales-list/sales-list.component';
import { SalesCreateComponent } from './sales-create/sales-create.component';
import { MainSalesComponent } from './main-sales/main-sales.component';


const routes: Routes = [
{
  path:'',
  component: MainSalesComponent,
  children: [
    {
      path: 'sales-list', 
      component: SalesListComponent
    },
    {
      path: 'sales-create', 
      component: SalesCreateComponent
    }
  ]
}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class SalesRoutingModule { }
