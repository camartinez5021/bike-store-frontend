import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { Observable } from 'rxjs';
import { ISales } from './interface/sales';
import { environment } from 'src/environments/environment';
import { map } from 'rxjs/operators';
import { IClients } from '../clients/interface/clients';
import { createRequestParams } from 'src/app/utils/request.utils';

@Injectable({
  providedIn: 'root'
})
export class SalesService {

  constructor(private http: HttpClient) { }

  public query(): Observable<ISales[]> {
    return this.http.get<ISales[]>(`${environment.END_POINT}/api/sales`)
    .pipe(map(res => {
      return res;
    }));
  }

  public getBikeById(id: string): Observable<IClients> {
    return this.http.get<IClients>(`${environment.END_POINT}/api/clients/${id}`)
      .pipe(map(res => {
        return res;
      }));

  }

  public deleteItem(id: String):Observable<ISales>{
    return this.http.delete<ISales>(`${environment.END_POINT}/api/sales/${id}`)
    .pipe(map(res => {
      return res;
    }));
  }

  public getClientsByDocument(id: string): Observable<IClients>{
    let params = new HttpParams();
    params = params.append('id ', id);
    console.warn('PARAMS ', params);
    return this.http.get<IClients>(`${environment.END_POINT}/api/clients/find-by-document`,{params: params})
    .pipe(map(res => {
      return res;
    }));
  }
}
