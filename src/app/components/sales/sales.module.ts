import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SalesCreateComponent } from './sales-create/sales-create.component';
import { SalesListComponent } from './sales-list/sales-list.component';
import { SalesRoutingModule } from './sales-routing.module';
import { ReactiveFormsModule } from '@angular/forms';
import { MainSalesComponent } from './main-sales/main-sales.component';
import {AutoCompleteModule} from 'primeng/autocomplete';


@NgModule({
  declarations: [SalesCreateComponent, SalesListComponent, MainSalesComponent],
  imports: [
    CommonModule, 
    SalesRoutingModule, 
    ReactiveFormsModule,
    AutoCompleteModule

  ]
})
export class SalesModule { }
