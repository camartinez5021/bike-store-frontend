import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HeaderComponent } from './components/common/header/header.component';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { SlideComponent } from './components/slide/slide.component';
import { LoginComponent } from './auth/login/login.component';
import { AuthInterceptor } from './auth/guards/auth.guard';
import { AccessDeniedComponent } from './auth/access-denied/access-denied.component';
import { AuthSharedModule } from './auth/auth-shared/auth-shared.module';
import { ProfileComponent } from './components/common/profile/profile.component';


@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    SlideComponent,
    LoginComponent,
    AccessDeniedComponent,
    ProfileComponent
    
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    ReactiveFormsModule,
    FormsModule,
    NgbModule,
    BrowserAnimationsModule,
    FontAwesomeModule,
    AuthSharedModule,
  ],
  providers: [
    {
      provide: HTTP_INTERCEPTORS,
      useClass: AuthInterceptor,
      multi: true
    }
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
