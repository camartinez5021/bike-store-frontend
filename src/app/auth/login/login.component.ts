import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { ICredentials, Credentials } from '../auth-shared/models/credentials';
import { Router } from '@angular/router';
import { LoginService } from './login.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.styl']
})
export class LoginComponent implements OnInit {
  LoginFormGroup = this.fb.group({
    username: [''],
    password: [''],
    rememberMe: false
  })


  constructor(private fb: FormBuilder, 
    private router: Router,
    private loginService: LoginService) {}

  ngOnInit(): void {
  }

  login(): void {
    const credentials: ICredentials = new Credentials();

    credentials.username = this.LoginFormGroup.value.username;
    credentials.password = this.LoginFormGroup.value.password;
    credentials.rememberMe = this.LoginFormGroup.value.rememberMe;

    this.loginService.login(credentials)
    .subscribe((res: any) => {
      console.warn('DATA RESPONSE CONTROLLER ', res );
      //this.router.navigate(['/header']);
    },(error: any) => {
      console.warn('ERROR ', error);
      if (error.status === 401) {
        console.warn('Usuario o contraseña incorrectos');
      }
    });


  }

}
