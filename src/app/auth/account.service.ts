import { Injectable } from '@angular/core';
import { ReplaySubject, Observable, of } from 'rxjs';
import { Account } from './auth-shared/models/account.model';
import { HttpClient } from '@angular/common/http';
import { Router } from '@angular/router';
import { environment } from 'src/environments/environment';
import { catchError, tap, shareReplay } from 'rxjs/operators';
import { ICredentials } from './auth-shared/models/credentials';

@Injectable({
  providedIn: 'root'
})
export class AccountService {
  private authenticationState = new ReplaySubject<Account | null>(1);
  private userIdentity: Account | null = null;
  private accountCache?: Observable<Account | null>;

  constructor(
    private http: HttpClient, private router: Router  
  ) { }

  create(credentials: ICredentials): Observable<ICredentials> {
    return this.http.post<ICredentials>(`${environment.END_POINT}/api/account`, credentials);
  }

  identity(force?: boolean): Observable<Account | null> {
    if (!this.accountCache || force || !this.isAuthenticated()) {
      this.accountCache = this.fetch().pipe(catchError(()=>{
        return of(null);
      }),tap((account: Account | null) => {
        this.authenticate(account);
        if(account){
          this.router.navigate(['/header']);
        }
      }), 
      shareReplay()
      );
    }
    return this.accountCache;
  }

  authenticate(identity: Account | null): void {
    this.userIdentity = identity;
  }

  isAuthenticated(): boolean {
    return this.userIdentity !== null;
  }

  getUserName(): String {
    return this.userIdentity.firstName + " " + this.userIdentity.lastName;
  }

  getUserAuthorities(): String[] {
    return this.userIdentity.authorities;
  }

  getUserEmail(): String {
    return this.userIdentity.email;
  }

  getUserLogin(): String {
    return this.userIdentity.login;
  }

  getUseractivated(): boolean {
    return this.userIdentity.activated;
  }

  getAuthenticationState(): Observable<Account | null> {
    return this.authenticationState.asObservable();
  }

/**
 * 
 * @param authorities Verity rols to users
 */
  hasAnyAuthority(authorities: string[] | string): boolean {
    if(!this.userIdentity || !this.userIdentity.authorities){
      return false;
    }

    if (!Array.isArray(authorities)){
      authorities = [authorities];
    }


    return this.userIdentity.authorities.some((authority: string) => authorities.includes(authority));
  }

  private fetch(): Observable<Account> {
    return this.http.get<Account>(`${environment.END_POINT}/api/account`);
  }
}
