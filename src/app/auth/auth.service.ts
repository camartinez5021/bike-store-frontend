import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { ICredentials } from './auth-shared/models/credentials';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  constructor(private http: HttpClient) { }

  public login(credentials: ICredentials): Observable<any>{
    return this.http.post<any>(`${environment.END_POINT}/api/authenticate`, credentials, {observe: 'response'})
    .pipe(map(res => {
      console.warn('View Response ', res);
      const bearerToken = res.headers.get('Authorization');
      console.warn('Bearer TOKEN ', bearerToken);

      if (bearerToken && bearerToken.slice(0, 7) === 'Bearer ') {
        const jwt = bearerToken.slice(7, bearerToken.length);
        this.storeAuthenticationToken(jwt, credentials.rememberMe);
        return jwt;
      }
    }));
  }

  public logout(): Observable<any> {
    return new Observable(observe => {
      localStorage.removeItem('token');
      sessionStorage.removeItem('token')
      observe.complete();
    });
  }

  private storeAuthenticationToken(jwt: string, rememberMe: boolean): void {
    if (rememberMe) {
      localStorage.setItem('token', jwt);
    } else {
      sessionStorage.setItem('token', jwt);
    }
  }
}
