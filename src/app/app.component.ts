import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.styl']
})
export class AppComponent implements OnInit {
  title = 'Hello bikes-app';

  numberOne: number = 56;
  numberTwo: number = 10;
  result1: number;
  result2: number;
  result3: number;
  result4: number;

  constructor(){}

  ngOnInit(): void {
    this.sum();
    this.rest();
    this.mult();
    this.div();
  }

  private sum(){
    this.result1 = this.numberOne + this.numberTwo;
    console.log('result sum ', this.result1);
  }

  private rest(){
    this.result2 = this.numberOne - this.numberTwo;
    console.log('result rest  ', this.result2);
  }

  private mult(){
    this.result3 = this.numberOne * this.numberTwo;
    console.log('result mult ', this.result3);
  }

  private div(){
    this.result4 = this.numberOne / this.numberTwo;
    console.log('result div ', this.result4);
  }

}