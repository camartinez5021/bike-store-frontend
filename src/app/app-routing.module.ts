import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LoginComponent } from './auth/login/login.component';
import { HeaderComponent } from './components/common/header/header.component';
import { AccessDeniedComponent } from './auth/access-denied/access-denied.component';
import { Authority } from './auth/auth-shared/constants/authority.constants';
import { UserRouteAccesGuard } from './auth/guards/user-route-acces.guard';
import { ProfileComponent } from './components/common/profile/profile.component';


const routes: Routes = [
  {
    path: 'bikes',
    data: {
      authorities: [Authority.ADMIN]
    },
    canActivate: [UserRouteAccesGuard],
    loadChildren: () => import('./components/bikes/bikes.module')
    .then(module => module.BikesModule)
  },
  {
    path: 'sales',
    loadChildren: () => import('./components/sales/sales.module')
    .then(module => module.SalesModule)
  },
  {
    path: 'client',
    loadChildren: () => import('./components/clients/clients.module')
    .then(module => module.ClientsModule)
  },
  {
    path: 'accessdenied',
    component: AccessDeniedComponent
  },
  {
    path: 'header',
    data: {
      authorities: [Authority.ADMIN]
    },
    canActivate: [UserRouteAccesGuard],
    component: HeaderComponent
  },
  {
    path: 'profile',
    component: ProfileComponent
  },
  {
    path: 'login',
    component: LoginComponent
  },
  {
    path: '',
    redirectTo: 'login',
    pathMatch: 'full'
  }




];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
